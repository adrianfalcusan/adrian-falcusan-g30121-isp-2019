package falcusan.adrian.lab10.ex3;


import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by adrianfalcusan on 06/05/2019.
 */
public class Counter implements Runnable {
    static int counter = 1; // a global counter

    static ReentrantLock counterLock = new ReentrantLock(true); // enable fairness policy

    static void incrementCounter(){
        counterLock.lock();

        // Always good practice to enclose locks in a try-finally block
        try{
            System.out.println(Thread.currentThread().getName() + ": " + counter);
            counter++;
        }finally{
            counterLock.unlock();
        }
    }

    @Override
    public void run() {
        while(counter<200){
            incrementCounter();
        }
    }

    public static void main(String[] args) {
        Counter te = new Counter();
        Thread thread1 = new Thread(te);
        Thread thread2 = new Thread(te);

        thread1.start();
        thread2.start();
    }
}
package falcusan.adrian.lab9.ex2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by adrianfalcusan on 13/05/2019.
 */
class PressButton implements ActionListener {


    @Override
    public void actionPerformed(ActionEvent e) {
        Counter.presses++;
        Counter.number++;
        Counter.count.setText(null);
        Counter.count.setText(String.valueOf(Counter.number));
    }
}
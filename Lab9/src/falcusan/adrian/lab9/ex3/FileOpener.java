package falcusan.adrian.lab9.ex3;

/**
 * Created by adrianfalcusan on 13/05/2019.
 */
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class FileOpener extends JFrame {

    static JLabel lContinut;
    static JLabel lNumeFisier;
    static JButton OpenButton;
    static JTextArea tContinut;
    static JTextArea tNumeFisier;


    FileOpener(){
        setTitle("File reader.");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(1280,720);
        setVisible(true);
        setResizable(false);
    }

    private void init() {
        this.setLayout(null);

        lNumeFisier = new JLabel("Nume Fisier:");
        lNumeFisier.setBounds(10,10,80,20);

        tNumeFisier = new JTextArea();
        tNumeFisier.setBounds(10, 30, 1130, 20);

        OpenButton = new JButton("Deschide");
        OpenButton.setBounds(1150,30,100,20);

        OpenButton.addActionListener(new OpenButtonHandler());

        lContinut = new JLabel("Continut fisier:");
        lContinut.setBounds(10,70,500,20);

        tContinut = new JTextArea(20,20);

        JScrollPane scroll = new JScrollPane(tContinut);
        scroll.setBounds(10,100,1240,550);

        add(lNumeFisier);
        add(tNumeFisier);
        add(lContinut);
        add(OpenButton);
        add(scroll);
    }
}



package falcusan.adrian.lab9.ex3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by adrianfalcusan on 13/05/2019.
 */
class OpenButtonHandler implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        String nume = FileOpener.tNumeFisier.getText();
        try {
            FileReader file = new FileReader(nume);
            BufferedReader reader = new BufferedReader(file);

            String input = reader.readLine();
            String current = reader.readLine();


            while(current!=null) {
                current = reader.readLine();
                input+=current;
                input+="\n";
            }

            FileOpener.tContinut.setText(input);
        } catch (FileNotFoundException e1) {
            FileOpener.tContinut.setText("Fisierul nu exista.");
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}

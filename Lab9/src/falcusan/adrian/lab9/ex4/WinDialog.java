package falcusan.adrian.lab9.ex4;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by adrianfalcusan on 13/05/2019.
 */
class WinDialog extends JFrame implements ActionListener {

    JLabel wins;
    JButton ok;


    public WinDialog(String winner){

        setTitle(winner+" is the winner!");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(250,150);
        this.setLayout(null);

        wins = new JLabel(winner+" player wins.");
        wins.setBounds(80,20,80,20);

        ok = new JButton("Ok");
        ok .setBounds(85,50,70,30);
        ok .addActionListener(this);

        add(wins);
        add(ok);

        setVisible(true);
        setResizable(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        dispose();
    }
}

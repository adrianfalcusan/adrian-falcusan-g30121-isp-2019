package falcusan.adrian.lab2.ex1;

/**
 * Created by adrianfalcusan on 11/03/2019.
 */

import java.util.Scanner;

public class ex1 {
    public static void main(String args[])
    {
        int a, b, max;
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter Two Number : ");
        a = scan.nextInt();
        b = scan.nextInt();

        if(a>b)
        {
            max = a;
        }
        else
        {
            max = b;
        }

        System.out.print("Max= " +max);
    }
}
package falcusan.adrian.lab2.ex3;
import java.util.Scanner;
/**
 * Created by adrianfalcusan on 11/03/2019.
 */
public class ex3 {


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        // Declare the variables
        int a, b, i, j, flag;
        int count=0;

        // Ask user to enter lower value of interval
        System.out.printf("A: ");
        a = sc.nextInt(); // Take input

        // Ask user to enter upper value of interval
        System.out.printf("\nB: ");
        b = sc.nextInt(); // Take input

        // Print display message
        System.out.printf("\nPrime numbers between %d and %d are: ", a, b);

        // Traverse each number in the interval
        // with the help of for loop
        for (i = a; i <= b; i++) {
            // Skip 1 as1 is niether
            // prime nor composite
            if (i == 1) {
                continue;
            }

            // flag variable to tell
            // if i is prime or not
            flag = 1;

            for (j = 2; j <= i / 2; ++j) {
                if (i % j == 0) {
                    flag = 0;
                    break;
                }
            }

            // flag = 1 means i is prime
            // and flag = 0 means i is not prime
            if (flag == 1) {
                System.out.println(i);
                count++;
            }

        }
        System.out.println("Count: " + count);
    }

}

package falcusan.adrian.lab2.ex4;

/**
 * Created by adrianfalcusan on 11/03/2019.
 */
public class ex4 {
    public static void main(String args[]){
        int array[] = new int[]{10, 11, 88, 2, 12, 120};

        // Calling getMax() method for getting max value
        int max = getMax(array);
        System.out.println("Maximum Value is: "+max);

    }

    // Method for getting the maximum value
    public static int getMax(int[] inputArray){
        int maxValue = inputArray[0];
        for(int i=1;i < inputArray.length;i++){
            if(inputArray[i] > maxValue){
                maxValue = inputArray[i];
            }
        }
        return maxValue;
    }
}


package falcusan.adrian.lab2.ex6;
import java.util.Scanner;
/**
 * Created by adrianfalcusan on 11/03/2019.
 */
public class ex6 {

    public static void main(String[] args) {
            int num ;
            Scanner scan = new Scanner(System.in);
            System.out.print("Enter The Number : ");
            num = scan.nextInt();
            long factorial = multiplyNumbers(num);
            System.out.println("Factorial of " + num + " = " + factorial);
    }
    public static long multiplyNumbers(int num) {
            if (num >= 1)
                return num * multiplyNumbers(num - 1);
            else
                return 1;
    }
}

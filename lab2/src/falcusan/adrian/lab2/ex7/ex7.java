package falcusan.adrian.lab2.ex7;
import java.util.Scanner;
/**
 * Created by adrianfalcusan on 11/03/2019.
 */
public class ex7 {
    public static void main(String[] args) {
        int secretNumber,triesLeft=3;
        secretNumber = (int) (Math.random() * 99 + 1);
        Scanner sc = new Scanner(System.in);
        int guess;
        do {
            System.out.print("Enter a guess (1-10): ");
            guess = sc.nextInt();
            triesLeft--;
            if (guess == secretNumber)
                System.out.println("Your guess is correct. Congratulations!");
            else if (guess < secretNumber)
                System.out
                        .println("Your guess is smaller than the secret number.");
            else if (guess > secretNumber)
                System.out
                        .println("Your guess is greater than the secret number.");
        } while (triesLeft != 0);
        if (triesLeft == 0) System.out.println("You lost!");
    }
}


package falcusan.adrian.lab3.ex2;

/**
 * Created by adrianfalcusan on 18/03/2019.
 */
public class Circle {
    private double radius;
    private String color;

    public Circle() {
        this.radius = 1.0;
        this.color = "red";
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(String color) {
        this.color = color;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return 2 * Math.PI * radius;
    }

}
package falcusan.adrian.lab3.ex2;

/**
 * Created by adrianfalcusan on 18/03/2019.
 */
public class TestCircle {
    public static void main(String[] args) {
        Circle C1= new Circle();
        Circle C2= new Circle(20);
        Circle C3= new Circle("blue");
        double Area1 = C1.getArea();
        double Rad2 = C2.getRadius();
        System.out.println(Area1 + Rad2);
    }
}

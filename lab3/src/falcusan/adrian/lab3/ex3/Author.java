package falcusan.adrian.lab3.ex3;

/**
 * Created by adrianfalcusan on 18/03/2019.
 */
public class Author {
    private String authorName;
    private String authorEmailAdress;
    private char authorGender;

    public Author(String authorName, String authorEmailAdress, char authorGender){
        this.authorName = authorName;
        this.authorEmailAdress = authorEmailAdress;
        this.authorGender = authorGender;
    }

    public String getAuthorEmailAdress() {
        return authorEmailAdress;
    }

    public char getAuthorGender() {
        return authorGender;
    }

    public String getAuthorName() {

        return authorName;
    }

    public void setAuthorEmailAdress(String authorEmailAdress) {
        this.authorEmailAdress = authorEmailAdress;
    }

    public void ttoString() {
        System.out.println(this.authorName + "(" + this.authorGender + ") at " + this.authorEmailAdress);
    }
}

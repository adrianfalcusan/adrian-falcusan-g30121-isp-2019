package falcusan.adrian.lab3.ex4;

/**
 * Created by adrianfalcusan on 18/03/2019.
 */
public class TestMyPoint {
    public static void main(String[] args) {
        MyPoint P1 = new MyPoint();
        MyPoint P2 = new MyPoint(9, 1);
        MyPoint P3 = new MyPoint(1, 1);
        int x1 = P2.getX();
        int x2 = P2.getX();
        System.out.println("(" + x1 + "," + x2 + ")");
        P3.ttoString();
        P3.setX(2);
        P3.setY(1);
        P3.ttoString();
        double dist1 = P1.distance(P2);
        System.out.println(dist1);
    }
}
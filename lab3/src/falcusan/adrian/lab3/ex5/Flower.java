package falcusan.adrian.lab3.ex5;

/**
 * Created by adrianfalcusan on 18/03/2019.
 */
public class Flower{
    int petal;
    static int count;

    Flower(){
        System.out.println("Flower has been created!");
        count++;
    }
public static int Count() {
        return count;
}

    public static void main(String[] args) {
        Flower[] garden = new Flower[5];
        for(int i =0;i<5;i++){
            Flower f = new Flower();
            garden[i] = f;
        }
        int c = Count();
        System.out.println(c);
    }
}

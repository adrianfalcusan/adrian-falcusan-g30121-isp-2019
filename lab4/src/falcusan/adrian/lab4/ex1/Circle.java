package falcusan.adrian.lab4.ex1;

import static java.lang.Math.pow;

/**
 * Created by adrianfalcusan on 18/03/2019.
 */

public class Circle {
    private double radius;
    private String color;
    private static final float PI = 3.1415f;

    public Circle(){
        this.radius = 1.0;
        this.color = "red";
    }

    public Circle(double radius, String color) {

        this.radius = radius;
        this.color = color;
    }

    public double getRadius(){
        System.out.println("radius = " + radius);
        return radius;
    }

    public double getArea(){
        System.out.println("area=" + PI*pow(radius,2));
        return PI*pow(radius,2);
    }
}

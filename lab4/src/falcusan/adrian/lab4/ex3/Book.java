package falcusan.adrian.lab4.ex3;

import falcusan.adrian.lab4.ex2.Author;

/**
 * Created by adrianfalcusan on 25/03/2019.
 */
public class Book extends Author {
    private static String name;
    private Author author;
    private double price;
    private int qtyInStock = 0;

    public Book(String name,String email, char gender, Author author, double price){
        super(name, email, gender);
        this.name = name;
        this.author = author;
        this.price = price;
    }

    public Book(String name, String email, char gender, Author author, double price, int qtyInStock){
        super(name, email, gender);
        this.name = name;
        this.author = author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String getName() {

        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public String toString(){
        System.out.println("book-" + getName() + " by " + super.toString());
        return null;
    }

}

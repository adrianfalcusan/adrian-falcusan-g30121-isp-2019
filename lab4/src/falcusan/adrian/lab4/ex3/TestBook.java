package falcusan.adrian.lab4.ex3;

import falcusan.adrian.lab4.ex2.Author;

/**
 * Created by adrianfalcusan on 01/04/2019.
 */
public class TestBook {
    public static void main(String[] args) {
        Author A1 = new Author("Jon Bon","jon@bon.org",'m');
        Book B1 = new Book("Bokk Nokb", "asfa@sfa.ro", 'm');
        System.out.println(B1.toString());
    }
}
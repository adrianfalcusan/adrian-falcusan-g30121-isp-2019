package falcusan.adrian.lab4.ex4;

import falcusan.adrian.lab4.ex2.Author;
import falcusan.adrian.lab4.ex3.Book;
import falcusan.adrian.lab4.ex3.Book2;

/**
 * Created by adrianfalcusan on 01/04/2019.
 */
public class TestBook2 {
    public static void main(String[] args)
    {
        Author[] authors = new Author[2];
        authors[0] = new Author("X X", "fsd@fdsa.da", 'm');
        authors[1] = new Author("Y Y", "fsss@fsssa.da", 'm');
        Book2 b2 = new Book2("Cart1", "saf@fdas.ro",'m', authors, 3.3);
        b2.printAuthors();

    }
}

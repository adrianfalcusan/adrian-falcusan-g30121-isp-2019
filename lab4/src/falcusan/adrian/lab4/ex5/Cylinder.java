package falcusan.adrian.lab4.ex5;

import falcusan.adrian.lab4.ex1.Circle;

/**
 * Created by adrianfalcusan on 25/03/2019.
 */
public class Cylinder extends Circle {
    private double height;

    public Cylinder() {
        this.height = 1.0;
    }

    public Cylinder(double radius, String color){
        super( radius, color);
    }

    public Cylinder(double radius, String color, double height) {
        super(radius, color );
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume(){
        return height*super.getArea();
    }
}

package falcusan.adrian.lab4.ex6;

import static java.lang.Math.pow;

/**
 * Created by adrianfalcusan on 25/03/2019.
 */
public class Circle extends Shape{
    private double radius;
    private String color;
    private static final float PI = 3.1415f;

    public Circle(){
        this.radius = 1.0;
        this.color = "red";
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius(){
        return radius;
    }

    public double getArea(){
        return PI*pow(radius,2);
    }
    public String toString(){
        System.out.println("A shape with color"+super.color+" and"+super.filled);
        return null;
    }
}
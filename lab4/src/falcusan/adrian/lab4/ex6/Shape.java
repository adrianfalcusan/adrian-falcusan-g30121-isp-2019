package falcusan.adrian.lab4.ex6;

/**
 * Created by adrianfalcusan on 25/03/2019.
 */
public class Shape {
    public String color;
    public boolean filled;

    public Shape() {
        this.color = "red";
        this.filled = true;
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public String toString(){
        if (filled) System.out.println("A Shape with color of" + color + " and" + "filled");
        else System.out.println("A Shape with color of" + color + " and" + " not filled");
        return null;
    }
}
package falcusan.adrian.lab4.ex6;

/**
 * Created by adrianfalcusan on 25/03/2019.
 */
public class Square extends Rectangle {


    public Square() {
    }

    public Square(double side) {
        super(side, side);
    }

    public Square(String color, boolean filled, double side) {
        super(color, filled, side, side);
    }

    public double getSide(){
        return super.width;
    }

    @Override
    public void setWidth(double side){
        width = side;
    }

    @Override
    public void setLength(double side){
        length = side;
    }

    @Override
    public String toString(){
        System.out.println("A square with side of:"+getSide()+"w hich is a subclass of:"+super.toString());
        return null;
    }
}

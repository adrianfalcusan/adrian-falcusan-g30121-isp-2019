package falcusan.adrian.lab5.ex1;

/**
 * Created by adrianfalcusan on 25/03/2019.
 */
public abstract class Shape {
    protected static String color;
    protected static boolean filled;

    public Shape() {
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    abstract void metoda();

    public static String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public static boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public double getArea() {
        return 0;
    }

    public double getPerimeter(){
        return 0;
    }

    public String toString(){
        return null;
    }
}


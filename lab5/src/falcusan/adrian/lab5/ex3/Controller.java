package falcusan.adrian.lab5.ex3;

import java.util.concurrent.TimeUnit;

/**
 * Created by adrianfalcusan on 01/04/2019.
 */
public class Controller {

    public void control() throws InterruptedException{
        short count = 0;
        while(count< 20) {
            TimeUnit.SECONDS.sleep(1);
            System.out.println("TempSensor:"+TemperatureSensor.getTempSensor()+" si LightSensor:"+LightSensor.getKightSensor());
            count++;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Controller c = new Controller();
        c.control();
    }
}

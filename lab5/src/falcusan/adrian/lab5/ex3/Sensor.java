package falcusan.adrian.lab5.ex3;

/**
 * Created by adrianfalcusan on 01/04/2019.
 */

public abstract class Sensor {
    private String location;

    public int readValue(){
        return 0;
    }

    public String getLocation(){
        return location;
    }

}

package falcusan.adrian.lab5.ex3;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by adrianfalcusan on 01/04/2019.
 */
public class TemperatureSensor extends Sensor {
    public static int tempSensor;

    public static int getTempSensor(){
        tempSensor =  ThreadLocalRandom.current().nextInt(0,101);
        return tempSensor;
    }
}
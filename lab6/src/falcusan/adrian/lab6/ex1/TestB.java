package falcusan.adrian.lab6.ex1;

/**
 * Created by adrianfalcusan on 01/04/2019.
 */
public class TestB {
    public static void main(String[] args) {
        BankAccount ba1 = new BankAccount("Robert",100);
        BankAccount ba2 = new BankAccount("Romario",100);
        BankAccount ba3 = new BankAccount("Mario",50);
        BankAccount ba4 = new BankAccount("Robert",100);
        if(ba1.equals(ba2))
            System.out.println(ba1+" , "+ba2+ " are equals");
        else
            System.out.println(ba1+" , "+ba2+ " aren't equals");
        if(ba1.equals(ba3))
            System.out.println(ba1+" , "+ba3+ " are equals");
        else
            System.out.println(ba1+" , "+ba3+ " aren't equals");
        if(ba1.equals(ba4))
            System.out.println(ba1+" , "+ba4+ " are equals");
        else
            System.out.println(ba1+" , "+ba4+ " aren't equals");
        System.out.println(ba1.hashCode());
        System.out.println(ba2.hashCode());
    }
}

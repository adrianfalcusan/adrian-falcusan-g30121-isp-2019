package falcusan.adrian.lab6.ex2;

/**
 * Created by adrianfalcusan on 08/04/2019.
 */
public class TestAccount {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Robert",1000);
        bank.addAccount("Romario",500);
        bank.addAccount("Mircea",250);
        bank.addAccount("Mario",100);
        bank.addAccount("Fernando",50);
        bank.addAccount("George",10);

        System.out.println("Print Accounts sorted by balance");
        bank.printAccounts();

        System.out.println("Print Accounts between limits");
        bank.printAccounts(100,500);

        System.out.println("Get Account by owner's name");
        bank.getAccount("Robert");

        System.out.println("Get accounts ordered by name");
        bank.getAllAccounts();

    }
}

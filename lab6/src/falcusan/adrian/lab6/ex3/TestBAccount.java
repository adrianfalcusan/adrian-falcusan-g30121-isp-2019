package falcusan.adrian.lab6.ex3;

/**
 * Created by adrianfalcusan on 08/04/2019.
 */
public class TestBAccount {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Robert",1000);
        bank.addAccount("Romario",500);
        bank.addAccount("Mircea",250);
        bank.addAccount("Mario",50);
        bank.addAccount("Fernando",100);
        bank.addAccount("George",10);

        System.out.println("Print Accounts sorted by balance");
        bank.printAccounts();

        System.out.println("Print Accounts between limits");
        bank.printAccounts(51, 499);

        System.out.println("Get Account by owner name and balance");
        bank.getAccount("Radu",500);

        System.out.println("Get All Accounts ");
        bank.getAllAccounts();

    }
}


package falcusan.adrian.lab6.ex4;

/**
 * Created by adrianfalcusan on 08/04/2019.
 */

public class Definition {
    private String description;

    public Definition(String description) {
        this.description = description;
    }
    public String toString() {
        return description;
    }
}
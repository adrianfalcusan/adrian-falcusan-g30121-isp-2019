package falcusan.adrian.lab6.ex4;
import java.util.*;
import static java.lang.System.*;

/**
 * Created by adrianfalcusan on 08/04/2019.
 */
public class Dictionary {
    HashMap dict = new HashMap();

    public void addWord(Word w, Definition d) {
        if (dict.containsKey(w))
            out.println("Modidy existent word!");
        else
            out.println("Add new word.");
        dict.put(w, d);
    }

    public Object getDefinition(Word w) {
        out.println("Search " + w);
        out.println(dict.containsKey(w));
        return dict.get(w);
    }

    public void afisDictionar() {
        out.println(dict);
    }

    public void getAllWords() {

        for (Object word : dict.keySet()) System.out.println(word.toString());
    }
    public void getAllDefinitions() {

        for (Object definition : dict.values()) System.out.println(definition.toString());
    }
}
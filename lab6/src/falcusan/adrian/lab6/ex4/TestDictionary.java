package falcusan.adrian.lab6.ex4;
import java.io.*;
/**
 * Created by adrianfalcusan on 08/04/2019.
 */
public class TestDictionary {
    public static void main(String[] args) throws IOException {

        Dictionary dict = new Dictionary();
        char raspuns;
        String linie;
        String explic;
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("Menu");
            System.out.println("a - Add");
            System.out.println("c - Search");
            System.out.println("w - Words");
            System.out.println("d - Defs");
            System.out.println("l - Dict");
            System.out.println("e - Close");

            linie = fluxIn.readLine();
            raspuns = linie.charAt(0);

            switch (raspuns) {
                case 'a': case 'A':
                    System.out.println("Word:");
                    linie = fluxIn.readLine();
                    if (linie.length() > 1) {
                        System.out.println("Def:");
                        explic = fluxIn.readLine();
                        dict.addWord(new Word(linie), new Definition(explic));
                    }
                    break;
                case 'c': case 'C':
                    System.out.println("Word:");
                    linie = fluxIn.readLine();
                    if (linie.length() > 1) {
                        Word x = new Word(linie);
                        explic = String.valueOf(dict.getDefinition(x));

                        if (explic == null)
                            System.out.println("Not found");
                        else
                            System.out.println("Def:" + explic);
                    }
                    break;
                case 'l': case 'L':
                    System.out.println("Display:");
                    dict.afisDictionar();
                    break;
                case 'w': case 'W':
                    System.out.println("Get the words");
                    dict.getAllWords();
                    break;
                case 'd': case 'D':
                    System.out.println("Get definitions");
                    dict.getAllDefinitions();
                    break;

            }
        } while (raspuns != 'e' && raspuns != 'E');
        System.out.println("Done");



    }

}

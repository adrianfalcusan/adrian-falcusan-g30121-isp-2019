package falcusan.adrian.lab7.ex2;

import java.util.Scanner;
import java.io.*;

/**
 * Created by adrianfalcusan on 08/04/2019.
 */

public class CountChar {
    public static void main(String args[]) throws Exception {
        Scanner scan = new Scanner(System.in);
        System.out.println("Introduceti un caracter:");

        String inString = scan.nextLine();
        String filePath = "data.txt";
        String strLine;

        int count = 0;

        try {
            FileInputStream fstream = new FileInputStream(filePath);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));

            while ((strLine = br.readLine()) != null) {
                strLine = strLine + " ";
                String[] strArry = strLine.split(inString);

                if (strArry.length > 1) {
                    count = count + strArry.length - 1;
                } else {
                    if (strLine == inString) {
                        count++;
                    }
                }
            }

            in.close();

            System.out.println("Caracterul a fost gasit de:"+count+" ori.");

        } catch (Exception e) {
            System.out.println("Eroare fisier.");
        }
    }
}

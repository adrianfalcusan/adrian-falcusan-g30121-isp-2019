package falcusan.adrian.lab7.ex3;

import java.io.*;
import java.util.Scanner;

/**
 * Created by adrianfalcusan on 15/04/2019.
 */
class test{
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);

        System.out.println("Introduceti numele fisierului pt encriptat:");
        String fileName = scan.nextLine();

        FileReader file = new FileReader(fileName+".dec");
        Encrypt.action(file,fileName);

        System.out.println("Introduceti numele fisierului pt decriptat:");
        fileName = scan.nextLine();

        FileReader file2 = new FileReader(fileName+".enc");
        Decrypt.action(file2,fileName);
    }
}

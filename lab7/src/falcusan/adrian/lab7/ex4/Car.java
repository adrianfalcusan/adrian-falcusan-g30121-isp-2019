package falcusan.adrian.lab7.ex4;
import java.io.*;

/**
 * Created by adrianfalcusan on 15/04/2019.
 */
public class Car implements Serializable {
    public String model;
    public int price;

    public Car(String model, int price){
        this.model = model;
        this.price = price;
    }

    public boolean equals(Car car){
        if(this.model.equals(car.model)) return true;
        else return false;
    }
}

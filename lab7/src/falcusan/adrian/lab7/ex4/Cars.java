package falcusan.adrian.lab7.ex4;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adrianfalcusan on 15/04/2019.
 */
public class Cars implements Serializable {
    public String nume;
    List<Car> list = new ArrayList<Car>();

    public Cars(String nume) {
        this.nume = nume;
    }

    public void add(Car car) {
        this.list.add(car);
    }

    public void save(String fileName) {
        try {
            ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(fileName));
            o.writeObject(this);
        } catch (IOException e) {
            System.err.println("Eroare salvare fisier.");
            e.printStackTrace();
        }
    }

    public void viewCars() {
        for (Car car : list) {
            System.out.println("Model:" + car.model + "Pret:" + car.price);
        }
    }

    public void searchCar(Car carS){
        for (Car car : list) {
            if(car.equals(carS)) System.out.println("Masina cautata este :" + car.model + "( " + car.price+" $ )");
        }
    }

}


package falcusan.adrian.lab7.ex4;

import java.util.Scanner;

/**
 * Created by adrianfalcusan on 15/04/2019.
 */
public class Test {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);


        System.out.println("Furnizor:");
        String Furnizor = scan.nextLine();
        Cars furnizor = new Cars(Furnizor);


        Car c1 = new Car("solenza", 59000);
        furnizor.add(c1);
        Car c2 = new Car("logan", 7000);
        furnizor.add(c2);
        Car c3 = new Car("1310", 1500);
        furnizor.add(c3);


        furnizor.viewCars();
        furnizor.save(Furnizor);


        furnizor.searchCar(c1);
    }
}

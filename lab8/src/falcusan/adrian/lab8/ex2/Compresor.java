package falcusan.adrian.lab8.ex2;

/**
 * Created by adrianfalcusan on 15/04/2019.
 */
public class Compresor {
    boolean stare;

    void pornesteCompresor(){
        stare = true;
        System.out.println("Porneste compresor.");
    }

    void opresteCompresor(){
        stare = false;
        System.out.println("Opreste compresor.");
    }

    void afiseaza(){
        System.out.println("Starea compresorului este "+stare);
    }
}


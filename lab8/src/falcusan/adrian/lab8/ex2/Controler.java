package falcusan.adrian.lab8.ex2;

/**
 * Created by adrianfalcusan on 15/04/2019.
 */
public class Controler {

    static final int V = 20;

    Senzor s;
    Compresor c;

    Controler(Senzor s, Compresor c){
        this.s = s;
        this.c = c;
    }

    void control(){
        if(s.valoare>V)
            c.pornesteCompresor();
        else
            c.opresteCompresor();
    }
}
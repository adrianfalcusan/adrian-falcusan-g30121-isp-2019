package falcusan.adrian.lab8.ex3;

/**
 * Created by adrianfalcusan on 15/04/2019.
 */
class Train{
    String destination;
    String name;

    public Train(String destinatie, String nume) {
        super();
        this.destination = destinatie;
        this.name = nume;
    }

    String getDestination(){
        return destination;
    }

}
package falcusan.adrian.lab8.ex4;

/**
 * Created by adrianfalcusan on 06/05/2019.
 */
public class AlarmUnit {
    public static void raiseAlarm(){
        System.out.println("Alarm is raised.");
        GsmUnit.callOwner();
    }
}

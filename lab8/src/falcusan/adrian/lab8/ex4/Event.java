package falcusan.adrian.lab8.ex4;

/**
 * Created by adrianfalcusan on 15/04/2019.
 */
abstract class Event {

    EventType type;

    Event(EventType type) {
        this.type = type;
    }

    EventType getType() {
        return type;
    }

}
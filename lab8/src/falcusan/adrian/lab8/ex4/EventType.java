package falcusan.adrian.lab8.ex4;

/**
 * Created by adrianfalcusan on 15/04/2019.
 */
enum EventType {
    TEMPERATURE, FIRE, NONE;
}

package falcusan.adrian.lab8.ex4;

/**
 * Created by adrianfalcusan on 15/04/2019.
 */
class FireEvent extends Event {

    private boolean smoke;

    FireEvent(boolean smoke) {
        super(EventType.FIRE);
        this.smoke = smoke;
    }

    boolean isSmoke() {
        return smoke;
    }

    @Override
    public String toString() {
        return "FireEvent{" + "smoke=" + smoke + '}';
    }

}
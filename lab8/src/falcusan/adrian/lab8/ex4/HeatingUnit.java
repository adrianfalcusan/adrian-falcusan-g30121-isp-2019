package falcusan.adrian.lab8.ex4;

/**
 * Created by adrianfalcusan on 06/05/2019.
 */
public class HeatingUnit {
    public static void turnOn(int temp, int threshold){

        System.out.println("Heating unit is turned on.");
        while(temp<threshold)
        {
            System.out.println("Current temperature:"+temp);
            temp++;
        }
        System.out.println("Temperature threshold reached. Heating unit is turning off.");
    }
}

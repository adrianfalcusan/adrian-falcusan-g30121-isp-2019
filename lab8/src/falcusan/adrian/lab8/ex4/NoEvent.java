package falcusan.adrian.lab8.ex4;

/**
 * Created by adrianfalcusan on 15/04/2019.
 */
class NoEvent extends Event{

    NoEvent() {
        super(EventType.NONE);
    }

    @Override
    public String toString() {
        return "NoEvent{}";
    }
}